package cn.lpq.magnetsearch;

import java.util.List;

public abstract class AbstractMagentSearch {

	public abstract List<MagentInfo> magentSearch(String keyword);
	
}
