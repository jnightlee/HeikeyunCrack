package cn.lpq.magnetsearch.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hutool.json.JSONUtil;
import cn.lpq.magnetsearch.AbstractMagentSearch;
import cn.lpq.magnetsearch.MagentInfo;

/**
 * http://www.btyunsou.cc 这个网站的磁力搜索
 * 
 * @author Administrator
 *
 */
public class BtyunsouMagentSearch extends AbstractMagentSearch {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String queryUrl = "http://www.btyunsou.cc/search?kw=";

	private List<MagentInfo> magentInfolist = new ArrayList<MagentInfo>();

	@Override
	public List<MagentInfo> magentSearch(String keyword) {
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse response = null;
		magentInfolist.clear();
		try {
			keyword = URLEncoder.encode(keyword, "UTF-8");
			httpClient = HttpClients.createDefault();
			HttpClientContext context = HttpClientContext.create();
			HttpGet httpget = new HttpGet(queryUrl + keyword);
			response = httpClient.execute(httpget, context);
			String locationUrl = context.getRedirectLocations().get(0).toString();
			Connection.Response execute = Jsoup.connect(locationUrl).ignoreContentType(true).execute();
			// 获取返回数据
			String body = execute.body();
			Document doc = Jsoup.parse(body);
			Elements links = doc.select("li.media");
			for (Element link : links) {
				String fileName = link.select("a.title").text();
				String magent = "magnet:?xt=urn:btih:"
						+ link.select("a.title").attr("href").replace("/", "").replace(".html", "");
				String fileSize = link.select("span.label-warning").text();
				MagentInfo magentInfo = new MagentInfo(fileName, fileSize, magent);
				magentInfolist.add(magentInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("解析发生错误！！！");
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return magentInfolist;
	}

	public static void main(String[] args) {
		AbstractMagentSearch s = new BtyunsouMagentSearch();
		System.out.println(JSONUtil.toJsonStr(s.magentSearch("阿丽塔")));
	}

}
